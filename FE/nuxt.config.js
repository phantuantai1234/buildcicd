export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'FE',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      {name: 'google-site-verification', content: "m20iIcy4bGCaemOjhalqllr2dr6O0y9_ublM9mDeFKM"}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // {src: './plugins/vue-google-oauth2', ssr: false},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    // '@nuxt/content'
  ],

  axios: {
    proxy: true
  },

  proxy: {
    '/api': 'https://asia-northeast2-advancedsearch-296908.cloudfunctions.net/TestCICD_1',
    '/token' : 'https://www.googleapis.com/oauth2/v4/token'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  server: {
    port: process.env.PORT || 3000,
    host: "0.0.0.0",
    timing: false
  }
}
