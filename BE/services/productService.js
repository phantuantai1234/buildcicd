const db = require("../db/dataAccess");
const FileDA = require('../utils/FileDA');

module.exports.GetAllProduct = async () => {
    process.env['SSL'] == undefined ? process.env['SSL'] = JSON.stringify(await FileDA.ReadSSL()) : "";
    let ssl = JSON.parse(process.env['SSL']);
    
    const sequelize = await db.CreateConnection(ssl.cKey, ssl.cCert, ssl.cCa);

    var data = await db.GetAllProduct(sequelize);
    return data;
} 