var fs = require('fs');
const path = require('path');

function ReadSSL()
{
    return new Promise(async (resolve, reject) => {
        ccert =  await fs.readFileSync(path.join(__dirname, '../config/ssl/client-cert.pem'));
        ckey =  await fs.readFileSync(path.join(__dirname, '../config/ssl/client-key.pem'));
        cca = await fs.readFileSync(path.join(__dirname, '../config/ssl/server-ca.pem'));

        let ssl = {
            cCert: ccert.toString(),
            cKey: ckey.toString(),
            cCa : cca.toString()
        }

        resolve(ssl);
    })
}

module.exports.ReadSSL = ReadSSL;