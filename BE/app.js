const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const app = express();
app.use(cors({credentials: true, origin: true, exposedHeaders: '*'}));
app.options('*', cors())
app.use(urlencodedParser);
app.use(jsonParser);
app.options('/', cors())
app.get('/', cors(), (req,res) => {
	res.json("OK");
})
app.use('/api', require('./api'))

module.exports = app;
