
const { Sequelize, QueryTypes } = require('sequelize');
const sqlConst  = require("../config/sqlConst");
const db = require('../config/sequalize');

module.exports.CreateConnection = (ckey, ccert, cca) => {
    return new Promise((resolve, reject) => {
        const sequelize = new Sequelize(db.connectionString.databaseName, db.connectionString.userName, db.connectionString.password,{
            host: db.connectionString.host,
            //port: 3306,
            dialect: 'mysql'
        });
        resolve(sequelize);
    })
}

module.exports.GetAllUser = (sequelize) => {
    return new Promise(async (resolve, reject) => {
        let data = await sequelize.query(sqlConst.selectAllUsers,{type : QueryTypes.SELECT});
        resolve(data);
    })
}

module.exports.GetAllProduct = (sequelize) => {
    return new Promise(async (resolve, reject) => {
        let data = await sequelize.query(sqlConst.selectAllProducts,{type : QueryTypes.SELECT}).catch((err) => console.log(JSON.stringify(err)));
        console.log("List products: " + JSON.stringify(data));
        resolve(data);
    }).catch((err) => console.log(JSON.stringify(err)))
}
