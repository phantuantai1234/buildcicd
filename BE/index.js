const app = require('./app');
var FileDA = require('./utils/FileDA');

function App(req,res) {
    if(!req.url){
        req.url = '/';
        req.path = '/';
    }

    return app(req,res);
}

const myBackEnd = App;

module.exports = {
    myBackEnd
}