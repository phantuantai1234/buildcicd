const router = require("express").Router();
const productService = require("../services/productService");

router.get("/products", async (req, res) => {
  let data = await productService.GetAllProduct();

  res.json(data);
});

router.get("/", (req, res) => {
  res.json("OK");
});

module.exports = router;
